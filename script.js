let n = +prompt('Enter a number, please');

while (!n || Number.isNaN(n)) {
    n = +prompt('Error! Enter a number, please');
}

function fibonacci(n) {
    let num1 = 1, num2 = 1;
    let num3 = 0;
    for (let i = 3; i <= n; i++) {
        num3 = num1 + num2;
        num1 = num2;
        num2 = num3;
    }
    console.log(num3);
    return num3;
}

fibonacci(n);